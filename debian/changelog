libgringotts (1:1.2.1-16) unstable; urgency=medium

  * debian/compat: update to 10.
  * debian/copyright:
    - Update Source url and copyright year for debian files.
  * debian/control:
    - Update to debhelper 10.
    - Bump to Standards-Version 4.1.2. No changes required.
    - Drop unnecessary dh-autoreconf build-dependency.
    - Refresh homepage and development urls.
  * debian/watch:
    - Rewrite to look at Github and update to version 4 format.
  * debian/rules:
    - Remove '--parallel' and '--with autoreconf' as they are enabled by
      default in compat level 10.

 -- Jose G. López <josgalo@gmail.com>  Sat, 09 Dec 2017 15:49:09 +0100

libgringotts (1:1.2.1-15) unstable; urgency=medium

  * Add patches to fix some problems:
    - 01_fix_signedness_errors.patch: Fix pointers casted from long to
      unsigned int. Thanks to Aurelien Jarno.
    - 02_fix_documentation_examples.patch: Fix examples in documentation.
    - 03_fix_testsuite.patch: Generated password is sometimes of inferior
      quality. (Closes: #736938).
  * Let build test suite again on s390x and hurd-i386.

 -- Jose G. López <josgalo@gmail.com>  Sat, 01 Feb 2014 13:00:11 +0100

libgringotts (1:1.2.1-14) unstable; urgency=low

  * Avoid building test suite to fix FTBFS on s390x and hurd-i386.

 -- Jose G. López <josgalo@gmail.com>  Mon, 27 Jan 2014 20:33:59 +0100

libgringotts (1:1.2.1-13) unstable; urgency=low

  * Bump Debian version to avoid getting rejected due to a previous
    version (1.2.1-12). (Closes: #728027).
  * Bump Standards-Version to 3.9.5. No changes required.

 -- Jose G. López <josgalo@gmail.com>  Sun, 15 Dec 2013 15:37:32 +0100

libgringotts (1:1.2.1-2) unstable; urgency=low

  * debian/control:
    - Bump Standards-Version to 3.9.4. No changes required.
    - Update debhelper to 9.
    - Add Multi-Arch support.
  * debian/rules:
    - Add hardening flags and clean deprecated code.
    - Add '--parallel' compilation.
  * debian/*.install files adapted to use Multi-Arch paths.
  * Add 00_fix_pkgconfig_pc_file.patch to fix paths for Multi-Arch.
  * Add doc-base control file to register documentation with doc-base.
  * Upgrade upstream's GPL version to be compatible with debian packaging.

 -- Jose G. López <josgalo@gmail.com>  Tue, 14 May 2013 20:30:59 +0200

libgringotts (1:1.2.1-1) unstable; urgency=low

  * Standards version 3.9.3.
  * Move to source version 3.0 (quilt).
  * Split libgringotts from gringotts source package again. It's an
    independent library and could be useful in other software.
  * Change version to match upstream's (1.2.1). Previous was 1.2.10~pre3-1
    due to merge on gringotts source package.
  * debian/control:
    - Bump to Standards-Version 3.9.3.
    - Add dh-autoreconf to Build-Depends (build system too old).
  * debian/rules: enable security hardening build flags.
  * debian/copyright: rewrite to machine-readable format.
  * Add symbols control file.
  * Stop shipping .la files (Closes: #633167).

 -- Jose G. López <josgalo@gmail.com>  Sat, 04 Aug 2012 11:46:21 +0200

libgringotts (1.2.10~pre3-1) unstable; urgency=low

  * New upstream release
  * Merge libgringotts source package
  * Update debhelper compat to 7
  * Use dh_lintian instead of ad-hoc lintian override installation
  * Remove build-dependency on libgdk-pixbuf-dev (closes: #516638)
  * Clean up libgringotts2 conflicts
  * Add Vcs-Git information
  * Update to Debian Policy 3.8.1 (no changes necessary)
  * Don't install suid root by default anymore (closes: #440789)

 -- Wesley J. Landaker <wjl@icecavern.net>  Wed, 22 Apr 2009 19:14:12 -0600

libgringotts (1.2.1-12) unstable; urgency=low

  * New maintainer, adopting orphaned package (closes: #434517)
  * Updated Homepage field to point to current development site
  * Fixed watch file (closes: #449843)
  * Cleaned up package short description
  * Updated copyright information to include all authors
  * Rewrote debian/rules; no longer using cdbs or dpatch
  * Updated to Debian Policy 3.7.3

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 11 Feb 2008 12:33:25 -0700

libgringotts (1.2.1-11) unstable; urgency=low

  * Orphaning this package: upstream homepage has vanished and the
    author does not respond to mails anymore.
  * Set maintainer to QA group
  * Replace ${Source-Version} with ${binary:Version}.

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 24 Jul 2007 16:31:04 +0200

libgringotts (1.2.1-10) unstable; urgency=low

  * Standards version 3.7.2.0
  * Conflict with libmhash << 0.9.6-2, since this version fixes the
    big-endian problems.
  * Remove libmudflap stuff, it is unused.
  * Rename libgringotts1 to libgringotts2 (Closes: #367262)

 -- Bastian Kleineidam <calvin@debian.org>  Fri, 19 May 2006 20:42:05 +0200

libgringotts (1.2.1-9) unstable; urgency=low

  * Patch to work with new libmhash2 0.9.4a library, and add a conflict
    to old versions << 0.9.4.
  * Use debhelper v5
  * Put Section: header in first stanza of debian/control

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 19 Jan 2006 20:43:13 +0100

libgringotts (1.2.1-8) unstable; urgency=low

  * Only enable mudflap support on debug builds. Else the performance
    penalty is too high.

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 22 Aug 2005 21:09:20 +0200

libgringotts (1.2.1-7) unstable; urgency=high

  * Don't build with mudflap support on mips/mipsel CPUs. Fixes FTBFS,
    thus urgency high.

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 11 Jul 2005 14:22:53 +0200

libgringotts (1.2.1-6) unstable; urgency=low

  * rerun libtoolize to fix build failure on GNU/k*BSD (Closes: #317296)

 -- Bastian Kleineidam <calvin@debian.org>  Thu,  7 Jul 2005 16:46:11 +0200

libgringotts (1.2.1-5) unstable; urgency=low

  * Bump up standards version to 3.6.2.1
  * Compile with gcc 4.0 and mudflap support, see also here:
    http://gcc.fyxm.net/summit/2003/mudflap.pdf

 -- Bastian Kleineidam <calvin@debian.org>  Wed,  6 Jul 2005 12:02:23 +0200

libgringotts (1.2.1-4) unstable; urgency=low

  * Remove 'powerful' from description. Now this packages uses less
    power and therefore saves some energy.

 -- Bastian Kleineidam <calvin@debian.org>  Mon,  9 May 2005 16:31:39 +0200

libgringotts (1.2.1-3) unstable; urgency=low

  * Fix typos in description (Closes: #304642)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 21 Apr 2005 13:05:20 +0200

libgringotts (1.2.1-2) unstable; urgency=low

  * added debian/watch file
  * use cdbs to build the package
  * update Standards version to 3.6.1

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  3 Aug 2004 23:05:43 +0200

libgringotts (1.2.1-1) unstable; urgency=low

  * New upstream release.

 -- Bastian Kleineidam <calvin@debian.org>  Fri, 25 Apr 2003 01:54:47 +0200

libgringotts (1.2.0-1) unstable; urgency=low

  * Initial Release. (Closes: #189299)

 -- Bastian Kleineidam <calvin@debian.org>  Wed, 16 Apr 2003 15:40:04 +0200

